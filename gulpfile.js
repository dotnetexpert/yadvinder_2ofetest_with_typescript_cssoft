var gulp = require('gulp');
var ts = require('gulp-typescript');
// var sass = require('gulp-sass');
// var browserSync = require('browser-sync').create();
 
gulp.task('serve', function () {
	
	
	
	return gulp.src('./app/**/*.ts')
		.pipe(ts({
			noImplicitAny: true,
			out: 'allTypeScriptFiles.js'
		}))
		.pipe(gulp.dest('build'));
});
// gulp.task('sass', function() {
//   return gulp.src('all.scss') // Gets all files ending with .scss in app/scss and children dirs
//     .pipe(sass())
//     .pipe(gulp.dest('./lib/css'))
// });

// gulp.task('serve', function() {
//   browserSync.init({
//     server: {
//       baseDir: '',     
//     },
//   })
// });