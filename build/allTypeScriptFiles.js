var main = angular.module("app.profile", ["ngMaterial", "ui.router", "LocalStorageModule"]);
main.config(routeConfig);
routeConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider.state("/", {
        url: "/",
        templateUrl: "/app/profile/profile.html",
        controller: "ProfileController as vm"
    });
    $urlRouterProvider.otherwise("/");
}
main.run(apprun);
apprun.$inject = ["localStorageService"];
function apprun(localStorageService) {
    var data = [
        {
            "username": "Garry Muster",
            "userid": 1,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Josan Muster",
            "userid": 2,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Carl Muster",
            "userid": 3,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Danielle Muster",
            "userid": 4,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Andrew Muster",
            "userid": 5,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "James Muster",
            "userid": 6,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Jane Muster",
            "userid": 7,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Joyce Muster",
            "userid": 8,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Katherine Muster",
            "userid": 9,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        },
        {
            "username": "Vincent Muster",
            "userid": 10,
            "company": "Zippel Media",
            "job": "Developer",
            "location": "Elsdorf", "province": "North-Rhine-Westfalia",
            "avatar": "./images/avatars/NoImage.jpg",
            "type": "user"
        }
    ];
    if (!localStorageService.get("profileUsers"))
        localStorageService.set("profileUsers", JSON.stringify(data));
}
var app;
(function (app) {
    var domain;
    (function (domain) {
        var Profile = (function () {
            function Profile(userid, username, company, job, location, province, avatar, type) {
                this.userid = userid;
                this.username = username;
                this.company = company;
                this.job = job;
                this.location = location;
                this.province = province;
                this.avatar = avatar;
                this.type = type;
            }
            return Profile;
        }());
        domain.Profile = Profile;
    })(domain = app.domain || (app.domain = {}));
})(app || (app = {}));
var app;
(function (app) {
    var profile;
    (function (profile) {
        var ProfileController = (function () {
            function ProfileController($scope, $mdDialog) {
                this.$scope = $scope;
                this.$mdDialog = $mdDialog;
                var vm = this.$scope;
                this.$scope.$on("UpdateCall", function (event, args) {
                    vm.$broadcast('UpdateCallDetail', args);
                });
                this.$scope.$on("DetailCall", function (event, args) {
                    vm.$broadcast('DetailCallDetail', args);
                });
                this.$scope.$on("RefreshProfileList", function (event, args) {
                    vm.$broadcast('RefreshProfileListData', args);
                });
            }
            ProfileController.prototype.call = function (args) {
                this.$scope.$broadcast('Detataatat', args);
            };
            ProfileController.$inject = ["$scope", "$mdDialog"];
            return ProfileController;
        }());
        angular
            .module("app.profile")
            .controller("ProfileController", ProfileController);
    })(profile = app.profile || (app.profile = {}));
})(app || (app = {}));
var app;
(function (app) {
    var AddUser;
    (function (AddUser) {
        var AddUserController = (function () {
            function AddUserController($rootScope, $mdDialog, zmUserService) {
                this.$rootScope = $rootScope;
                this.$mdDialog = $mdDialog;
                this.zmUserService = zmUserService;
                var vm = this.$rootScope;
            }
            AddUserController.prototype.addUser = function (name) {
                var _this = this;
                if (name) {
                    var profile = {
                        "username": name,
                        "userid": 0,
                        "company": "Zippel Media",
                        "job": "Developer",
                        "location": "Elsdorf", "province": "North-Rhine-Westfalia",
                        "avatar": "./images/avatars/NoImage.jpg",
                        "type": "user"
                    };
                    this.zmUserService.addUser(profile).then(function (profiles) {
                        _this.$rootScope.$broadcast('RefreshProfileListData', "");
                    }).catch(function (reason) {
                        console.log("something went wrong!");
                    });
                    this.$mdDialog.hide();
                }
            };
            AddUserController.prototype.closeDialog = function () {
                this.$mdDialog.hide();
            };
            AddUserController.$inject = ["$rootScope", "$mdDialog", "zmUserService"];
            return AddUserController;
        }());
        angular
            .module("app.profile")
            .controller("AddUserController", AddUserController);
    })(AddUser = app.AddUser || (app.AddUser = {}));
})(app || (app = {}));
var app;
(function (app) {
    var Service;
    (function (Service) {
        "use strict";
        var ZmUserRestService = (function () {
            function ZmUserRestService($http, localStorageService) {
                this.$http = $http;
                this.localStorageService = localStorageService;
                this.serviceBase = "";
            }
            ZmUserRestService.prototype.readListOfUser = function () {
                var profileUsers = this.localStorageService.get("profileUsers");
                return this.$http.get('contacts.json').then(function (response) { return profileUsers; });
            };
            ZmUserRestService.prototype.readUser = function (userId) {
                return this.$http.get(this.serviceBase + userId).then(function (response) { return response.data; });
            };
            ZmUserRestService.prototype.deleteUser = function (userId) {
                this.profileuser = this.localStorageService.get('profileUsers');
                this.profileuser.splice(userId, 1);
                this.localStorageService.set('profileUsers', this.profileuser);
                return this.$http.get('contacts.json').then(function (response) { return response; });
                //return  this.$http.delete(this.serviceBase+userId).then(response => response.data);
            };
            ZmUserRestService.prototype.addUser = function (user) {
                this.profileuser = this.localStorageService.get('profileUsers');
                user.userid = this.profileuser.length + 1;
                this.profileuser.unshift(user);
                this.localStorageService.set('profileUsers', this.profileuser);
                return this.$http.get('contacts.json').then(function (response) { return response; });
                //   return  this.$http.post(this.serviceBase,user).then(response => response.data);
            };
            ZmUserRestService.prototype.readPersonalDetails = function (userId) {
                return this.$http.get(this.serviceBase + userId).then(function (response) { return response.data; });
            };
            ZmUserRestService.prototype.updatePersonalDetails = function (user, index) {
                this.profileuser = this.localStorageService.get('profileUsers');
                this.profileuser.splice(index, 1);
                this.profileuser.splice(index, 0, user);
                this.localStorageService.set('profileUsers', this.profileuser);
                return this.$http.get('contacts.json').then(function (response) { return response; });
                //  return  this.$http.put(this.serviceBase,user).then(response => response.data);
            };
            ZmUserRestService.prototype.readCompanyDetails = function (userId) {
                // return  this.$http.get('contacts.json').then(response => profileUsers);
                return this.$http.get(this.serviceBase + userId).then(function (response) { return response.data; });
            };
            ZmUserRestService.prototype.updateCompanyDetails = function (user, index) {
                this.profileuser = this.localStorageService.get('profileUsers');
                this.profileuser.splice(index, 0, user);
                this.localStorageService.set('profileUsers', this.profileuser);
                return this.$http.get('contacts.json').then(function (response) { return response; });
                // return  this.$http.put(this.serviceBase,user).then(response => response.data);
            };
            ZmUserRestService.$inject = ["$http", "localStorageService"];
            return ZmUserRestService;
        }());
        Service.ZmUserRestService = ZmUserRestService;
        angular.module("app.profile").service("zmUserRestService", ZmUserRestService);
    })(Service = app.Service || (app.Service = {}));
})(app || (app = {}));
var app;
(function (app) {
    var Service;
    (function (Service) {
        "use strict";
        var ZmUserService = (function () {
            function ZmUserService(zmUserRestService) {
                this.zmUserRestService = zmUserRestService;
            }
            ZmUserService.prototype.readListOfUser = function () {
                return this.zmUserRestService.readListOfUser().then(function (response) { return response; });
            };
            ZmUserService.prototype.readUser = function (userId) {
                return this.zmUserRestService.readUser(userId).then(function (response) { return response; });
            };
            ZmUserService.prototype.deleteUser = function (userId) {
                return this.zmUserRestService.deleteUser(userId).then(function (response) { return response; });
            };
            ZmUserService.prototype.addUser = function (user) {
                return this.zmUserRestService.addUser(user).then(function (response) { return response; });
            };
            ZmUserService.prototype.readPersonalDetails = function (userId) {
                return this.zmUserRestService.readPersonalDetails(userId).then(function (response) { return response; });
            };
            ZmUserService.prototype.updatePersonalDetails = function (user, index) {
                return this.zmUserRestService.updatePersonalDetails(user, index).then(function (response) { return response; });
            };
            ZmUserService.prototype.readCompanyDetails = function (userId) {
                return this.zmUserRestService.readCompanyDetails(userId).then(function (response) { return response; });
            };
            ZmUserService.prototype.updateCompanyDetails = function (user, index) {
                return this.zmUserRestService.updateCompanyDetails(user, index).then(function (response) { return response; });
            };
            ZmUserService.$inject = ["zmUserRestService"];
            return ZmUserService;
        }());
        Service.ZmUserService = ZmUserService;
        angular.module("app.profile").service("zmUserService", ZmUserService);
    })(Service = app.Service || (app.Service = {}));
})(app || (app = {}));
var app;
(function (app) {
    var Components;
    (function (Components) {
        var ZmProfileUserController = (function () {
            function ZmProfileUserController($scope, $mdDialog, zmUserService) {
                var _this = this;
                this.$scope = $scope;
                this.$mdDialog = $mdDialog;
                this.zmUserService = zmUserService;
                this.textBinding = 'hfgdshjfjh';
                this.dataBinding = 0;
                this.profileList = [];
                var vm = this;
                zmUserService.readListOfUser().then(function (profiles) {
                    _this.profileList = profiles;
                    // console.log(profiles);            
                }).catch(function (reason) {
                    console.log("something went wrong!");
                });
                this.gridBind = function () {
                    zmUserService.readListOfUser().then(function (profiles) {
                        vm.profileList = profiles;
                        console.log(profiles);
                    }).catch(function (reason) {
                        console.log("something went wrong!");
                    });
                };
                this.$scope.$on("RefreshProfileListData", function (event, args) {
                    vm.gridBind();
                });
            }
            ZmProfileUserController.prototype.edit = function (ev, profile, index) {
                profile.index = index;
                this.$scope.$emit("UpdateCall", profile);
                event.stopPropagation();
            };
            ZmProfileUserController.prototype.detail = function (ev, profile, index) {
                profile.index = index;
                this.$scope.$emit("DetailCall", profile);
                event.stopPropagation();
            };
            ZmProfileUserController.prototype.AddUserDialog = function (ev) {
                var parentEl = angular.element(document.body);
                this.$mdDialog.show({
                    parent: parentEl,
                    templateUrl: '/app/profile/dialogs/add-user/add-user.html',
                    targetEvent: ev,
                    clickOutsideToClose: false
                });
            };
            ZmProfileUserController.$inject = ["$scope", "$mdDialog", "zmUserService"];
            return ZmProfileUserController;
        }());
        var ZmProfileUser = (function () {
            function ZmProfileUser() {
                this.bindings = {
                    textBinding: '@',
                    dataBinding: '<',
                    functionBinding: '&'
                };
                this.controller = ZmProfileUserController;
                this.templateUrl = '/app/profile/componentCreated/user/components/profileUser/profileUser.html';
            }
            return ZmProfileUser;
        }());
        angular.module('app.profile').component('zmProfileUser', new ZmProfileUser());
    })(Components = app.Components || (app.Components = {}));
})(app || (app = {}));
var app;
(function (app) {
    var Components;
    (function (Components) {
        var ZmProfileUserDetailController = (function () {
            function ZmProfileUserDetailController($scope, zmUserService) {
                this.$scope = $scope;
                this.zmUserService = zmUserService;
                this.editContact = false;
                this.textBinding = 'hfgdshjfjh';
                this.dataBinding = 0;
                this.selected = false;
                this.editContact = false;
                var vm = this;
                this.$scope.$on("UpdateCallDetail", function (event, args) {
                    console.log(args);
                    vm.selected = true;
                    vm.editContact = true;
                    vm.contact = args;
                    vm.selectedData = [];
                });
                this.$scope.$on("DetailCallDetail", function (event, args) {
                    console.log(args);
                    vm.selected = true;
                    vm.editContact = false;
                    vm.selectedData = args;
                    vm.contact = [];
                    this.index = args.index;
                    localStorage.setItem("id", args.index);
                });
            }
            ZmProfileUserDetailController.prototype.add = function () {
                //this.functionBinding();
                this.$scope.$emit("handleEmit", "hello");
            };
            ZmProfileUserDetailController.prototype.delete = function (ev, data) {
                var _this = this;
                this.zmUserService.deleteUser(localStorage.getItem("id")).then(function (profiles) {
                    _this.$scope.$emit("RefreshProfileList", "hello");
                }).catch(function (reason) {
                    console.log("something went wrong!");
                });
            };
            ZmProfileUserDetailController.prototype.update = function (ev, data, index) {
                var _this = this;
                this.selected = false;
                this.editContact = false;
                console.log(data);
                this.zmUserService.updatePersonalDetails(data, localStorage.getItem("id")).then(function (profiles) {
                    _this.$scope.$emit("RefreshProfileList", "hello");
                }).catch(function (reason) {
                    console.log("something went wrong!");
                });
            };
            ZmProfileUserDetailController.$inject = ["$scope", "zmUserService"];
            return ZmProfileUserDetailController;
        }());
        var ZmProfileUserDetail = (function () {
            function ZmProfileUserDetail() {
                this.bindings = {
                    textBinding: '@',
                    dataBinding: '<',
                    functionBinding: '&'
                };
                this.controller = ZmProfileUserDetailController;
                this.templateUrl = '/app/profile/componentCreated/user/components/profileUserDetail/profileUserDetail.html';
            }
            return ZmProfileUserDetail;
        }());
        angular.module('app.profile').component('zmProfileUserDetail', new ZmProfileUserDetail());
    })(Components = app.Components || (app.Components = {}));
})(app || (app = {}));
