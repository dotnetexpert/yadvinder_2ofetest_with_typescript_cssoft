module app.AddUser{

 interface IAddUserController  {
        addUser(args:any) : void;
        closeDialog():void;
        
    }
    class AddUserController implements IAddUserController{
       
        public static $inject = ["$rootScope","$mdDialog","zmUserService"];
        constructor(public $rootScope:ng.IRootScopeService,public $mdDialog: angular.material.IDialogService,private zmUserService:app.Service.ZmUserService) {
            var vm=this.$rootScope;
                
                  
        }
       addUser(name){
            if(name){
                var profile = {
                    "username": name,
                    "userid":0,
                    "company": "Zippel Media",
                    "job": "Developer",
                    "location": "Elsdorf", "province": "North-Rhine-Westfalia",
                    "avatar": "./images/avatars/NoImage.jpg",
                    "type": "user"
                }
                
                this.zmUserService.addUser(profile).then(profiles => {
                   
                        this.$rootScope.$broadcast('RefreshProfileListData',"");           
                }).catch( reason => {
                console.log("something went wrong!");
                });
                        
                this.$mdDialog.hide();
        } 
       }       
    
    closeDialog(){
        this.$mdDialog.hide();
    }
    }
    angular
		.module("app.profile")
		.controller("AddUserController",
			AddUserController);
    
}