module app.profile{

 interface IProfileController  {
        call(args:any) : void;
        
    }
    class ProfileController implements IProfileController{
       
        public static $inject = ["$scope","$mdDialog"];
        constructor(public $scope:ng.IScope,public $mdDialog: angular.material.IDialogService) {
            var vm=this.$scope;
                 this.$scope.$on("UpdateCall", function (event, args) {                 
                      vm.$broadcast('UpdateCallDetail',args);
                  });     
                   this.$scope.$on("DetailCall", function (event, args) {                 
                      vm.$broadcast('DetailCallDetail',args);
                  }); 
                   this.$scope.$on("RefreshProfileList", function (event, args) {                 
                      vm.$broadcast('RefreshProfileListData',args);
                  }); 
                  
        }
       call(args){           
           this.$scope.$broadcast('Detataatat',args);
        }        
    
    }
    angular
		.module("app.profile")
		.controller("ProfileController",
			ProfileController);
    
}