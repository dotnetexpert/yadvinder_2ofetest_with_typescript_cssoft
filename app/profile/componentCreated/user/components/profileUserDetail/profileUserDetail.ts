module app.Components {
 
    interface IZmProfileUserDetailBindings  {
        textBinding: string;
        dataBinding: number;
        functionBinding: () => any;
        
    }
 
    interface IZmProfileUserDetailController extends IZmProfileUserDetailBindings {
        add(): void;
        selected:boolean;
        selectedData:any;
        editContact:boolean;
        delete(ev:any,data:any):void;
        update(ev:any,data:any,index:number):void;
        index:number;
        contact:any;
    }
 
    class ZmProfileUserDetailController implements IZmProfileUserDetailController {
 
        public textBinding:string;
        public dataBinding:number;
        public functionBinding:() => any;
        public selected:boolean;
        public selectedData:any;
        public contact:any;
        public editContact=false;
        public index:number;
        
        public static $inject=["$scope","zmUserService"];        
       
        constructor(private $scope:ng.IScope,private zmUserService:app.Service.ZmUserService) {           
            this.textBinding = 'hfgdshjfjh';
            this.dataBinding = 0;
            this.selected=false;
            this.editContact=false;
            var vm=this;
            
             this.$scope.$on("UpdateCallDetail", function (event, args) {                
                    console.log(args);
                    vm.selected=true;
                    vm.editContact=true;
                    vm.contact=args;
                     vm.selectedData=[];
        });
             
           this.$scope.$on("DetailCallDetail", function (event, args) {                
                    console.log(args);
                    vm.selected=true;
                    vm.editContact=false;
                    vm.selectedData=args; 
                     vm.contact=[];                  
                    this.index=args.index;                   
                    localStorage.setItem("id",args.index);
        });  
        
              
        }
 
        
        add():void {
            //this.functionBinding();
            
            this.$scope.$emit("handleEmit","hello");
        }
        
        delete(ev,data)
        {
            
            this.zmUserService.deleteUser(localStorage.getItem("id")).then(profiles => {              
                this.$scope.$emit("RefreshProfileList","hello");
                }).catch( reason => {
                console.log("something went wrong!");
                });
           
           
        }
        update(ev,data,index)
        {
         
             this.selected=false;
             this.editContact=false;
            console.log(data);
             this.zmUserService.updatePersonalDetails(data,localStorage.getItem("id")).then(profiles => {              
                this.$scope.$emit("RefreshProfileList","hello");
                }).catch( reason => {
                console.log("something went wrong!");
                });
        }
       
 
    }
 
    class ZmProfileUserDetail implements ng.IComponentOptions {
 
        public bindings:any;
        public controller:any;
        public templateUrl:string;
 
        constructor() {
            this.bindings = {
                textBinding: '@',
                dataBinding: '<',
                functionBinding: '&'
            };
            this.controller = ZmProfileUserDetailController;
            this.templateUrl = '/app/profile/componentCreated/user/components/profileUserDetail/profileUserDetail.html';
        }
 
    }
 
    angular.module('app.profile').component('zmProfileUserDetail', new ZmProfileUserDetail());
 
}