module app.Components {
 
    interface IZmProfileUserBindings  {
        textBinding: string;
        dataBinding: number;
        functionBinding: () => any;
        
    }
 
    interface IZmProfileUserController extends IZmProfileUserBindings {    
        profileList: app.domain.IProfile[];
        edit(ev:any,profile:any,index):void;
        detail(ev,profile,index):void;
    }
 
    class ZmProfileUserController implements IZmProfileUserController {
 
        public textBinding:string;
        public dataBinding:number;
        public functionBinding:() => any;
        public profileList:app.domain.IProfile[];
        public gridBind:()=>void;
        public static $inject=["$scope","$mdDialog","zmUserService"];
        constructor(public $scope:ng.IScope,public $mdDialog: angular.material.IDialogService,private zmUserService:app.Service.ZmUserService) {           
            this.textBinding = 'hfgdshjfjh';
            this.dataBinding = 0;
            this.profileList=[];
            var vm=this;
            
               zmUserService.readListOfUser().then(profiles => {
                this.profileList = profiles;   
                // console.log(profiles);            
                }).catch( reason => {
                console.log("something went wrong!");
                });
                this.gridBind=function(){
                               
                   zmUserService.readListOfUser().then(profiles => {                      
                      vm.profileList = profiles;   
                       console.log(profiles);            
                       }).catch( reason => {
                         console.log("something went wrong!");
                           });
                };
                   this.$scope.$on("RefreshProfileListData", function (event, args) {  
                   vm.gridBind();
                      });  
        }
 
                
        edit(ev,profile,index):void{        
            profile.index=index;
            this.$scope.$emit("UpdateCall",profile);
            event.stopPropagation();
        }
        detail(ev,profile,index):void{
            profile.index=index;
            this.$scope.$emit("DetailCall",profile);
            event.stopPropagation();
        }
        
        AddUserDialog(ev) {
        var parentEl = angular.element(document.body);
        this.$mdDialog.show({
            parent: parentEl,   // use parent scope in template 
            templateUrl: '/app/profile/dialogs/add-user/add-user.html',
            targetEvent: ev,
            clickOutsideToClose: false
        });      
    }        
 
    }
 
    class ZmProfileUser implements ng.IComponentOptions {
 
        public bindings:any;
        public controller:any;
        public templateUrl:string;
 
        constructor() {
            this.bindings = {
                textBinding: '@',
                dataBinding: '<',
                functionBinding: '&'
            };
            this.controller = ZmProfileUserController;
            this.templateUrl = '/app/profile/componentCreated/user/components/profileUser/profileUser.html';
        }
 
    }
 
    angular.module('app.profile').component('zmProfileUser', new ZmProfileUser());
 
}