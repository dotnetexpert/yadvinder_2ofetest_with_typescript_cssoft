namespace app.Service {
	"use strict";

	export class ZmUserRestService {
           public serviceBase:string;
           public profileuser:any;
        static $inject=["$http","localStorageService"];
		constructor(private $http: ng.IHttpService,private localStorageService:angular.local.storage.ILocalStorageService) {
            this.serviceBase="";
            
		}
        
        
       public readListOfUser(): ng.IPromise<app.domain.IProfile[]> {  
           var profileUsers=  this.localStorageService.get("profileUsers");           
              return  this.$http.get('contacts.json').then(response => profileUsers);
       }
       
       public readUser(userId:number): ng.IPromise<app.domain.IProfile> {      
              return  this.$http.get(this.serviceBase+userId).then(response => response.data);
       }
       
        public deleteUser(userId:number): ng.IPromise<app.domain.IProfile> {  
              this.profileuser = this.localStorageService.get('profileUsers');                
                this.profileuser.splice(userId, 1);
                this.localStorageService.set('profileUsers', this.profileuser)   
             return  this.$http.get('contacts.json').then(response => response);         
              //return  this.$http.delete(this.serviceBase+userId).then(response => response.data);
       }
       
       public addUser(user:app.domain.IProfile): ng.IPromise<app.domain.IProfile> {  
             this.profileuser = this.localStorageService.get('profileUsers'); 
             user.userid=this.profileuser.length+1;               
                this.profileuser.unshift(user);
                this.localStorageService.set('profileUsers', this.profileuser)    
            return  this.$http.get('contacts.json').then(response =>response );        
            //   return  this.$http.post(this.serviceBase,user).then(response => response.data);
       }
       
       public readPersonalDetails(userId:number): ng.IPromise<app.domain.IProfile> {    
             return  this.$http.get(this.serviceBase+userId).then(response => response.data);
       }
       
       public updatePersonalDetails(user:app.domain.IProfile,index:number): ng.IPromise<app.domain.IProfile> {   
             this.profileuser = this.localStorageService.get('profileUsers');             
             this.profileuser.splice(index, 1);               
                this.profileuser.splice(index, 0,user);               
                this.localStorageService.set('profileUsers', this.profileuser)           
             return  this.$http.get('contacts.json').then(response => response);
            //  return  this.$http.put(this.serviceBase,user).then(response => response.data);
       }
       
       public readCompanyDetails(userId:number): ng.IPromise<app.domain.IProfile> {             
             // return  this.$http.get('contacts.json').then(response => profileUsers);
             return  this.$http.get(this.serviceBase+userId).then(response => response.data);
       }
       
       public updateCompanyDetails(user:app.domain.IProfile,index:number): ng.IPromise<app.domain.IProfile> {             
            this.profileuser = this.localStorageService.get('profileUsers');                
                this.profileuser.splice(index, 0,user);               
                this.localStorageService.set('profileUsers', this.profileuser)           
             return  this.$http.get('contacts.json').then(response => response);
             // return  this.$http.put(this.serviceBase,user).then(response => response.data);
       }
	}

	angular.module("app.profile").service("zmUserRestService",ZmUserRestService);
}